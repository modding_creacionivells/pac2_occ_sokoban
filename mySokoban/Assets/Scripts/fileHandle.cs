using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;
using TMPro;

public static class fileHandle 
{
    public static string saveToJSON<T>( List<T> toSave, string fileName)
    {   // Pre: Se llama desde grid_cs cuando desde ui se presiona el boton guardar
        // Post: guarda la informacion en un fichero JSON
        if (fileName.Equals(""))
            fileName = "saveData";
        string content = JsonHelper.ToJson<T>(toSave.ToArray());
        return writeFile(fileName,getPath(fileName), content);
    }

    public static List<T> readFromJSON<T>(string fileName)
    {   // Pre: Se llama desde grid_cs cuando desde ui se presiona el boton load
        // Post: carga la info del fichero selecionado
        List<T> myList = new List<T>();
        //Debug.Log(getPath(fileName));
        string content = readFile(getPath(fileName));
        if (!(string.IsNullOrEmpty(content) || content.Equals("{}")))
            myList = JsonHelper.FromJson<T>(content).ToList();
        return myList;
    }

    private static string getPath(string fileName)
    {   // Pre: se llama al cargar o guardar 
        // Post: devuelve el path del fichero.
        return Application.persistentDataPath + "/" + fileName + ".json";
    }

    private static string  writeFile(string fileName, string path, string content)
    {   // Pre: Se llama desde SaveToJSON
        // Post: guarda la informacion en un fichero JSON
        FileStream fileStream;
        try { fileStream = new FileStream(path, FileMode.Create); }
        catch
        {   // si el documento existe entonces cambianos el nombre de file
            string[] allFiles = Directory.GetFiles(Application.persistentDataPath);
            path = getPath(fileName + "(" + allFiles.Length + ")");
            fileStream = new FileStream(path, FileMode.Create);
        }
        Debug.Log(path);
        using (StreamWriter writer = new StreamWriter(fileStream))
        { writer.Write(content); }
        return path;
    }

    private static string readFile(string path)
    {   // Pre: Se llama desde readFromJSON
        // Post: lee la informacion de un fichero JSON
        string content = "";
        if (File.Exists(path))
        {
            using (StreamReader sr = new StreamReader(path))
            { content = sr.ReadToEnd(); }
        }
        return content;
    }
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.cells;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.cells = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.cells = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] cells;
    }
}