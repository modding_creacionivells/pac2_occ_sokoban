using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    public GameObject ui_objMenu;                       
    public GameObject ui_obSelect;                      
    public GameObject ui_showSelect;                      
    public GameObject ui_objSettings;                   
    public GameObject ui_objCredits;
    private saveData _saveData;
    private int lvlToSelect;

    private void Start()
    {
        lvlToSelect = 0;
        _saveData = new saveData();
        _saveData = saveManager.loadGameState();
    }

    // Pre: se llama cuando le das a play
    // Post: cambia a la partida de game
    public void goGameSscene() 
    {
        _saveData.sd_currentMap = 0;
        saveManager.SaveGameState(_saveData);
        SceneManager.LoadScene(2); // gameScene
    }

    // Pre: se llama cuando le das a buildMap
    // Post: cambia a la creacion de niveles
    public void goBuildSscene() { SceneManager.LoadScene(1); }

    // Pre: se llama cuando cierras los otros menus
    // Post: Muestra el Main menu 
    public void showMenu() { showCorrectUI(true, false, false, false, false); }

    // Pre: se llama cuando le das al boton de Mostrar nivel
    // Post: Muestra todos los niveles 
    public void showNivelesSelect() { showCorrectUI(false, false, true, false, false); }

    // Pre: se llama cuando le das al boton de selecionar nivel
    // Post: Muestra todos los mapas 
    public void showSelect(int _lvl) { lvlToSelect = _lvl; showCorrectUI(false, true, false, false, false); }

    public void setNewLevl(string nameLvl)
    {
        lvlToSelect = lvlToSelect - 1; // los botones van de 1 a 10 y esto va de 0 a 9
        switch (lvlToSelect)
        {
            case 0:
                _saveData.sd_map0Name = nameLvl;
                break;
            case 1:
                _saveData.sd_map1Name = nameLvl;
                break;
            case 2:
                _saveData.sd_map2Name = nameLvl;
                break;
            case 3:
                _saveData.sd_map3Name = nameLvl;
                break;
            case 4:
                _saveData.sd_map4Name = nameLvl;
                break;
            case 5:
                _saveData.sd_map5Name = nameLvl;
                break;
            case 6:
                _saveData.sd_map6Name = nameLvl;
                break;
            case 7:
                _saveData.sd_map7Name = nameLvl;
                break;
            case 8:
                _saveData.sd_map8Name = nameLvl;
                break;
            case 9:
                _saveData.sd_map9Name = nameLvl;
                break;
        }
        saveManager.SaveGameState(_saveData);
        showNivelesSelect();
    }

    public int getLvlToSelect() {  return lvlToSelect; }

    // Pre: se llama cuando le das al boton de settings
    // Post: Muestra los settings 
    public void showSettings() { showCorrectUI(false, false, false,true, false); GetComponent<GameSettings>().setAllValues(); }

    // Pre: se llama cuando le das al boton de credits
    // Post: Muestra los credits 
    public void showCredits() { showCorrectUI(false, false, false, false, true); }

    private void showCorrectUI(bool bMenu, bool bSelect, bool bSShowelect, bool bSettings, bool bCredts)
    {   // Pre: Se llama cuando se presiona un boton
        // Post: Muestra el UI correcto
        ui_objMenu.SetActive(bMenu);
        ui_obSelect.SetActive(bSelect);
        ui_showSelect.SetActive(bSShowelect);
        ui_objSettings.SetActive(bSettings);
        ui_objCredits.SetActive(bCredts);

        GetComponent<GameSettings>().updateTextLanguage();
    }

}
