using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System.IO;

// ###############################################################################################################
// ###### Modificacion   Fecha       Descripcion                                                            ######
// ###### ------------   ----------  ---------------------------------------------------------------------- ######
// ######     ++01       06/04/2024  Guardar en JSON                                                        ######
// ######     ++02       06/04/2024  Menu controllers                                                       ######
// ###############################################################################################################

public class grid_cs : MonoBehaviour
{
    private int width;
    private int height;
    private float cellSize;
    private int[,] gridArray;                                   // 0 ground, 1 wall, 2 goal, 3 prop (box), 4 player 
    [Header("Grid")]
    [SerializeField] private Tilemap tile_map;                  // all walls
    [SerializeField] private Tilemap tile_groundObj;            // ground
    [SerializeField] private Tile tileGround;
    [SerializeField] private Tile tileWall;
    [SerializeField] private Tile tileGoal;
    [SerializeField] private Tile tileBox;
    [SerializeField] private Tile tilePlayer;
    [SerializeField] private Vector3Int playerPos;
    [SerializeField] private bool existPlayer;

    [Header(" == JSON == ")]
    private List<cellData> cells = new List<cellData>();        // ++01 
    [SerializeField] private GameObject myFileName;             // ++01
    private bool isTest = false;

    public void newGrid(int width, int height, float cellSize)
    {   // Pre: al entrar al modo creaci�n inicializamos el mapa
        // Post: crea nuestro terreno con nada puesto solo el suelo
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        
        cells.Clear();                  // ++01

        gridArray = new int[width, height];
        existPlayer = false;// inicializamos la pos del player

        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                tile_groundObj.SetTile(new Vector3Int(x, y, 0), tileGround);
                gridArray[x, y] = 0;
            }
        }
    }

    public void setTileM(Vector3 worldPos, int gridValue)
    {   // Pre: Se llama en modo creaci�n cuando presionan con el raton.
        // Post: modifica el tileMap para poner el nuevo objeto en el tile
        int _x, _y;
        getXY(worldPos, out _x, out _y);
        if (_x >= 0 && _y >= 0 && _x < width && _y < height)
        {   // gridValue = 0 ground, 1 wall, 2 goal, 3 prop (box), 4 player 
            switch (gridValue)
            {
                case 0:
                    checkplayer(_x, _y);
                    tile_map.SetTile(new Vector3Int(_x, _y, 0), null);
                    break;
                case 1:
                    checkplayer(_x, _y);
                    tile_map.SetTile(new Vector3Int(_x, _y, 0), tileWall);
                    break;
                case 2:
                    checkplayer(_x, _y);
                    tile_map.SetTile(new Vector3Int(_x, _y, 0), tileGoal);
                    break;
                case 3:
                    checkplayer(_x, _y);
                    tile_map.SetTile(new Vector3Int(_x, _y, 0), tileBox);
                    break;
                case 4:
                    if (existPlayer)
                    {
                        tile_map.SetTile(new Vector3Int(playerPos.x, playerPos.y, 0), null);
                        gridArray[playerPos.x, playerPos.y] = 0;
                    }
                        
                    playerPos.x = _x;
                    playerPos.y = _y;
                    tile_map.SetTile(new Vector3Int(_x, _y, 0), tilePlayer);
                    existPlayer = true;
                    break;
            }
            gridArray[_x, _y] = gridValue;
        }
    }

    private void checkplayer(int x, int y) 
    {   // Pre: Se llama siempre que se modifica un tile
        // Post: cuando pones un objeto donde el jugador continuamos teniendo el jugador alli y al moverlo quitamos el objeto, por eso para evitar el error tenemos esta comprovacion
        if (playerPos.x == x && playerPos.y == y)
        {
            existPlayer = false;
            tile_map.SetTile(new Vector3Int(playerPos.x, playerPos.y, 0), null);
        }
            
    }

    private void getXY(Vector3 worldPos, out int x, out int y) 
    {   // Pre: se llama cada vez que se va a modificar el terreno
        // Post: obtiene la posicion de X y Y segun nuestro mapa.
        x = Mathf.FloorToInt(worldPos.x / cellSize);
        y = Mathf.FloorToInt(worldPos.y / cellSize);
    }

    #region saveData
    // ++01 ini
    public void saveLevel()
    {   // Pre: se llama cuando presionamos el boton de guardar
        // Post: guarda toda la informaci�n que tenemos en gridArray y lo guarda en el fichero JSON
        saveData _saveData = new saveData();
        _saveData = saveManager.loadGameState();
        string fileName;
        int indiceUltimoSeparador = 0;
        cells.Clear();
        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                cells.Add(new cellData(x,y, gridArray[x, y]));
            }
        }
        fileName = myFileName.GetComponent<TMP_InputField>().text;
        if (isTest)
            fileName = "myTestMap";

        fileName = fileHandle.saveToJSON<cellData>(cells, fileName);
        indiceUltimoSeparador = fileName.LastIndexOf('/');
        _saveData.sd_fileName = fileName.Substring(indiceUltimoSeparador + 1, fileName.LastIndexOf('.') - indiceUltimoSeparador - 1);
        if (isTest)
        {
            _saveData.sd_fileName = "myTestMap";
            _saveData.sd_currentMap = -1;
        }
        _saveData.sd_refresh = true;
        _saveData.sd_cellSize = cellSize;
        saveManager.SaveGameState(_saveData);
        if (isTest)
            SceneManager.LoadScene(2);  //gameScene
        else
            StartCoroutine(wait1second());
    }

    public void testLevel()
    {
        isTest = true;
        saveLevel();
    }

    public void loadLevel(string fileName)
    {   // Pre: se llama cuando presionamos el boton de load/cargar
        // Post: carga toda la informaci�n que tenemos en gridArray y lo guarda la lsita de celdas
        cells = fileHandle.readFromJSON<cellData>(fileName);
        refreshMap();
    }

    private void refreshMap()
    {
        for (int x = 0; x < cells.Count; x++)
        {
            switch (cells[x].value)
            {
                case 0:
                    tile_map.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), null);
                    break;
                case 1:
                    tile_map.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileWall);
                    break;
                case 2:
                    tile_map.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileGoal);
                    break;
                case 3:
                    tile_map.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileBox);
                    break;
                case 4:
                    playerPos.x = cells[x].x;
                    playerPos.y = cells[x].y;
                    tile_map.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tilePlayer);
                    existPlayer = true;
                    break;
            }
            gridArray[cells[x].x, cells[x].y] = cells[x].value;
        }
    }

    IEnumerator wait1second() 
    {
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    // ++01
    #endregion saveData
}
