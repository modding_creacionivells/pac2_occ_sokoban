using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class GameSettings : MonoBehaviour
{
    // Esta clase nos permite configurar las opciones del juego, guardando y modificando los valores que tenemos en la pantalla de settings

    public TMP_Dropdown languageDropDown;
    public Slider volumeSlider;
    public Slider brightnessSlider;
    public Image panelbrightness;
    private string mc_languageGame;
    private float mc_Volume;
    private float mc_brightness;
    private saveData _saveData;

    private void Awake()
    {
        _saveData = saveManager.loadGameState();                               // Carga los ajsutes del juego

        // load all data
        mc_languageGame = _saveData.sd_language;      // nos los guardamos en nuestras variables
        mc_Volume = _saveData.sd_Volume;
        mc_brightness = _saveData.sd_brightness;
        AudioListener.volume = mc_Volume;
        panelbrightness.gameObject.SetActive(true);                             // por defeceto tenemos el panel de brillo descativado, por que a la hora de trabajar molestaba
    }

    public void setAllValues()
    {
        volumeSlider.value = mc_Volume;                                         // assignamos a nustro volumen Slider el valor que tiene guardado
        AudioListener.volume = mc_Volume;                                       // assignamos a nustro volumenListener el valor que tiene guardado
        brightnessSlider.value = mc_brightness;                                 // assignamos a nustro Brillo Slider el valor que tiene guardado
        SetLanguageValues();
    }

    public void SetVolume(float volume)
    {   // esta funcion es llamada cuando movemos el valor del slider Volumen
        mc_Volume = volume;             // asignamos el valor a la variable de datos
        settingsSaveData();             // y luego guardamos la informacion en el fichero
    }

    public void Setbrightness(float brightness)
    {   // esta funcion es llamada cuando modificamos el valor del Slider Brillo
        mc_brightness = brightness;     // assignamos el valor de brillo
        settingsSaveData();             // guardamos el valor de brillo
    }


    public void SetLanguage(int languageIndex)
    {   // esta funcion es llamada cuando modificamos el valor de Dropdown de idioma
        if (languageIndex == 0)
        {
            //PlayerPrefs.SetString("languageGame", "ENG");
            mc_languageGame = "ENG";
        }
        else if (languageIndex == 1)
        {
            //PlayerPrefs.SetString("languageGame", "ESP");
            mc_languageGame = "ESP";
        }
        else if (languageIndex == 2)
        {
            //PlayerPrefs.SetString("languageGame", "CAT");
            mc_languageGame = "CAT";
        }
        else
        {
            //PlayerPrefs.SetString("languageGame", "ENG");
            mc_languageGame = "ENG";
        }
        settingsSaveData();
    }

    private void SetLanguageValues()
    {   // esta funcion nos rellena los campos que tiene el Dropdown de idioma
        List<string> LanguageList = new List<string>();
        string[] languageString = new string[3];
        int language = GetCurrentLanguage();

        languageString[0] = "English";
        languageString[1] = "Ingl�s";
        languageString[2] = "Ingl�s";
        LanguageList.Add(languageString[language]);

        languageString[0] = "spanish language";
        languageString[1] = "Espa�ol";
        languageString[2] = "Espanyol";
        LanguageList.Add(languageString[language]);

        languageString[0] = "Catalan language";
        languageString[1] = "Catalan";
        languageString[2] = "Catala";
        LanguageList.Add(languageString[language]);

        languageDropDown.ClearOptions();
        languageDropDown.AddOptions(LanguageList);

        if (mc_languageGame == "ENG")
        {
            languageDropDown.GetComponent<TMP_Dropdown>().SetValueWithoutNotify(0);
        }
        else if (mc_languageGame == "ESP")
        {
            languageDropDown.GetComponent<TMP_Dropdown>().SetValueWithoutNotify(1);
        }
        else if (mc_languageGame == "CAT")
        {
            languageDropDown.GetComponent<TMP_Dropdown>().SetValueWithoutNotify(2);
        }
        else
        {
            languageDropDown.GetComponent<TMP_Dropdown>().SetValueWithoutNotify(0);
        }

    }


    private int GetCurrentLanguage()
    {   // nos devuelve el idioma que tenemos actualmente
        int language = 0;

        if (mc_languageGame == "ENG")
        {
            language = 0;
        }
        else if (mc_languageGame == "ESP")
        {
            language = 1;
        }
        else if (mc_languageGame == "CAT")
        {
            language = 2;
        }
        else
        {   // english default language
            language = 0;
        }

        return language;
    }

    public void settingsSaveData()
    {   
        _saveData.sd_language = mc_languageGame;
        _saveData.sd_Volume = mc_Volume;
        _saveData.sd_brightness = mc_brightness;
        saveManager.SaveGameState(_saveData);

        AudioListener.volume = mc_Volume;
        panelbrightness.color = new Color(panelbrightness.color.r, panelbrightness.color.g, panelbrightness.color.b, mc_brightness);
        updateTextLanguage();
        SetLanguageValues();
    }

    public void updateTextLanguage()
    {   // esta actualiza todos los textos de la pantalla, asi que al cargar el Ui correcto luego llamamos a esta funcion para buscar todos los objetos con el tag txtText i los updateamos
        GameObject[] allTxt;
        allTxt = GameObject.FindGameObjectsWithTag("txtTag");

        foreach (GameObject currentText in allTxt)
        { currentText.GetComponent<txt_controller>().chosseText(); }
    }
}
