using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// ###############################################################################################################
// ###### Modificacion   Fecha       Descripcion                                                            ######
// ###### ------------   ----------  ---------------------------------------------------------------------- ######
// ######     ++01       06/04/2024  Menu controllers                                                       ######
// ###############################################################################################################
public class buildController : MonoBehaviour
{
    [Header("Map")]
    [SerializeField] private Camera myCamera;
    [SerializeField] private int width;
    [SerializeField] private int height;
    [SerializeField] private float cellSize;
    [SerializeField] private int numObject; // 0 ground, 1 wall, 2 goal, 3 prop (box), 4 player 

    [Header("Menu Controller")]                                 // ++01 inicio
    [SerializeField] private GameObject ui_GameUI;
    [SerializeField] private GameObject ui_exit;
    [SerializeField] private GameObject ui_loadSelect;
    [SerializeField] private GameObject ui_LoadConfirm;                                                  
    [SerializeField] private bool isPause;                      // ++01 final                                             
    private grid_cs myGrid;
    [SerializeField] private string fileNameToLoad;
    [SerializeField] private Image panelbrightness;

    // Start is called before the first frame update
    void Start()
    {
        isPause = false;
        numObject = 0;
        myGrid = GetComponent<grid_cs>();
        myGrid.newGrid(width, height, cellSize);

        saveData _saveData = new saveData();
        _saveData = saveManager.loadGameState();
        if (_saveData.sd_refresh)
        {
            fileNameToLoad = _saveData.sd_fileName;
            _saveData.sd_refresh = false;
            saveManager.SaveGameState(_saveData);
            confirmacionLoad();
        }
        AudioListener.volume = _saveData.sd_Volume;
        panelbrightness.gameObject.SetActive(true);
        panelbrightness.color = new Color(panelbrightness.color.r, panelbrightness.color.g, panelbrightness.color.b, _saveData.sd_brightness);
    }

    void Update()
    {
        
        if (Input.GetMouseButtonDown(0) && !isPause)
        {
            if ((Input.mousePosition.y < 865 || Input.mousePosition.y > 1030))   // ignoramos cuando estamos cambiando de objeto
                myGrid.setTileM(myCamera.ScreenToWorldPoint(Input.mousePosition), numObject);
        }   
    }

    // Pre: Se llama desde el UI de game, en especifico al presionar el boton de floor
    // Post: cambia el numObject que estamos poniendo en el grid
    public void floorSelected() { numObject = 0; }

    // Pre: Se llama desde el UI de game, en especifico al presionar el boton de wall
    // Post: cambia el numObject que estamos poniendo en el grid
    public void wallSelected() { numObject = 1; }

    // Pre: Se llama desde el UI de game, en especifico al presionar el boton de goal
    // Post: cambia el numObject que estamos poniendo en el grid
    public void goalSelected() { numObject = 2; }

    // Pre: Se llama desde el UI de game, en especifico al presionar el boton de box
    // Post: cambia el numObject que estamos poniendo en el grid
    public void boxSelected() { numObject = 3; }

    // Pre: Se llama desde el UI de game, en especifico al presionar el boton de player
    // Post: cambia el numObject que estamos poniendo en el grid
    public void playerSelected() { numObject = 4; }

    #region menuController
    // ++01 inicio
    // Pre: se llama al presionar el boton de exit
    // Post: llama uiControl y le dice que muestre el ui de exit
    public void openMenuExit() 
    { 
        uiControl(false, true, false, false);
        isPause = true;
        myCamera.GetComponent<camera_cs>().updateIsPause(isPause); 
    }

    // Pre: se llama al presionar el boton de exit
    // Post: llama uiControl y le dice que muestre el ui de exit
    public void closeUI() 
    { 
        uiControl(true, false, false, false);
        isPause = false;
        myCamera.GetComponent<camera_cs>().updateIsPause(isPause); 
    }

    // Pre: se llama al presionar el boton de load
    // Post: muestra el UI de los niveles, donde cada nombre de documento JSON guardado se mostrara (en ShowAllFiles.cs) y al presionar el botton se ejecuta "ButonAction.cs"
    public void loadBtt_1() { uiControl(false, true, true, false); }

    // Pre: se llama al presionar uno de los botones de fileNames que se esta mostrando en pantalla desde "loadBtt_1()
    // Post: nos envia el nombre del fichero selecionado y mostramos si deverdad quiere cargarlo
    public void loadBtt_2(string _fileNameButton) 
    {
        fileNameToLoad = _fileNameButton;
        uiControl(false, true, false, true); 
    }

    // Pre: se llama cuando confirmas que quieres cargar
    // Post: quita el ui y nos envia a grid_cs --> load que carga el mapa
    public void confirmacionLoad()
    {
        closeUI();
        GetComponent<grid_cs>().loadLevel(fileNameToLoad);
    }

    // Pre: se llama al presionar el boton de save
    // Post: llama el metodo de grid_cs --> saveLevel()
    public void saveButton() { GetComponent<grid_cs>().saveLevel(); closeUI(); }

    private void uiControl(bool bGameUI, bool bexit, bool bLoadSel, bool bLoadConf)
    {   // Pre: se llama al presionar botones de la escena de BuildMap
        // Post: segun el boton que presionemos se mostrara un UI o otro.   
        ui_GameUI.SetActive(bGameUI);
        ui_exit.SetActive(bexit);
        ui_loadSelect.SetActive(bLoadSel);
        ui_LoadConfirm.SetActive(bLoadConf);
    }

    public void goMenu()
    {
        SceneManager.LoadScene(0); // menu
    }
    // ++01 final
    #endregion menuController
}
