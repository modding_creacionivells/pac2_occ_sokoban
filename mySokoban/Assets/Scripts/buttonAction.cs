using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class buttonAction : MonoBehaviour
{
    [SerializeField] private GameObject gameController;
    [SerializeField] private bool isSelect;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(callLoadFile);
    }

    // Pre: Se llama al presionar un botton que se muestra en pantalla (son los file names)
    // Post: guarda el nombre del fichero y llama a la confirmacion de si quiere cargar o no el fichero.
    private void callLoadFile() 
    {
        if (!isSelect)
        {
            gameController = GameObject.Find("buildController");
            gameController.GetComponent<buildController>().loadBtt_2(transform.GetChild(0).GetComponent<TextMeshProUGUI>().text);
        }
        else
        {
            gameController = GameObject.Find("sceneManager");
            gameController.GetComponent<mainMenu>().setNewLevl(transform.GetChild(0).GetComponent<TextMeshProUGUI>().text);
        }
        
    }

    public void updateIsSelect() {  isSelect = true; }
}