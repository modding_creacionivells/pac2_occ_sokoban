using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxTriggers : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if ((Input.GetKey(KeyCode.W) && this.name == "down_trigger") || (Input.GetKey(KeyCode.S) && this.name == "top_trigger")
                || (Input.GetKey(KeyCode.D) && this.name == "left_trigger") || (Input.GetKey(KeyCode.A) && this.name == "right_trigger"))
                transform.parent.gameObject.GetComponent<boxItem>().boxTrigger(this.name);
        }
            
    }
}
