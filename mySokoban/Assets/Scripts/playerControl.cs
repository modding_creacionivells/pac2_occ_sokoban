using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControl : MonoBehaviour
{
    [SerializeField] private bool isWalking;    // dice si estamos caminando
    [SerializeField] private int movDir;        // 0 idle, 1 front, 2 back, 3 side

    private Animator _MyAnimator;
    private Rigidbody2D rbPlayer;
    private SpriteRenderer _SpriteRenderer;
    [SerializeField] private float m_speed;
    [SerializeField]Vector2 movement;

    // Start is called before the first frame update
    void Start()
    {
        isWalking = false;
        movDir = 0;
        _MyAnimator = GetComponent<Animator>();
        rbPlayer = GetComponent<Rigidbody2D>();
        _SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.S))
            movePlayerVars(1, true);
        else if (Input.GetKey(KeyCode.W))
            movePlayerVars(2, true);
        else if (Input.GetKey(KeyCode.A))
        {
            movePlayerVars(3, true);
            _SpriteRenderer.flipX = false;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _SpriteRenderer.flipX = true;
            movePlayerVars(3, true);
        }
        else
            movePlayerVars(0, false);

        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");
    }

    private void FixedUpdate()
    {
        // Apply this movement to the rigidbody's position
        rbPlayer.MovePosition(rbPlayer.position + movement * m_speed * Time.fixedDeltaTime);
    }

    private void LateUpdate()
    {
        _MyAnimator.SetInteger("movDir", movDir);
        _MyAnimator.SetBool("isWalking", isWalking);
    }

    private void movePlayerVars(int _myDir, bool _isWalk)
    {   // Pre: Se llama cada vez que el jugador realiza una acion
        // Pre: Actualiza los parametros que se envia al animator.
        movDir = _myDir;
        isWalking = _isWalk;
    }
}
