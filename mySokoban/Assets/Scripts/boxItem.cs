using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class boxItem : MonoBehaviour
{
    [SerializeField] private float distCheck;
    private RaycastHit2D hit;
    [SerializeField] private float m_speed;
    public LayerMask groundLayer;
    private Rigidbody2D myBody;
    private Vector2 newPos;
    [SerializeField]private bool isMoving;
    [SerializeField]private Vector3 posToMove;

    // Start is called before the first frame update
    void Start()
    {
        isMoving = false;
        myBody = GetComponent<Rigidbody2D>();
        newPos = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        //hit = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y), Vector2.right * movHor, frontEnemy);
        if (isMoving)
        {  //transform.position = Vector3.Lerp(transform.position, posToMove, m_speed * Time.deltaTime);
            if (transform.position == posToMove || Vector3.Distance(transform.position, posToMove) < 0.05f)
            {
                newPos = Vector2.zero;
                isMoving = false;
            }
        }
    }

    private void LateUpdate()
    {
        myBody.velocity = newPos * m_speed * Time.fixedDeltaTime;
    }

    private void OnDrawGizmos()
    {   // simplemente dibuja los Gizmos para que podamos ver que es lo que detecta o no.
        /*
        Gizmos.color = Color.red; // down
        Gizmos.DrawRay(transform.position, Vector2.down * distCheck);

        Gizmos.color = Color.green; // top
        Gizmos.DrawRay(transform.position, Vector2.up * distCheck);

        Gizmos.color = Color.white; // right
        Gizmos.DrawRay(transform.position, Vector2.right * distCheck);

        Gizmos.color = Color.blue; // left
        Gizmos.DrawRay(transform.position, Vector2.left * distCheck);
        */
    }

    public void boxTrigger(string nameTrigger)
    {   // Pre: Se llama cuando el player toca la caja
        // Post: identifica por donde lo ha tocado y luego comprueva si se puede mover y si puede lo mueve
        if (!isMoving)
        {
            if (nameTrigger == "top_trigger")
            {
                posToMove = new Vector3(transform.position.x, transform.position.y - distCheck);
                checkDir(Vector2.down);
            }
            else if (nameTrigger == "down_trigger")
            {
                posToMove = new Vector3(transform.position.x, transform.position.y + distCheck);
                checkDir(Vector2.up);
            } 
            else if (nameTrigger == "right_trigger")
            {
                posToMove = new Vector3(transform.position.x - distCheck, transform.position.y);
                checkDir(Vector2.left);
            }
            else
            {
                posToMove = new Vector3(transform.position.x + distCheck, transform.position.y);
                checkDir(Vector2.right);
            }
                
        }
    }

    private void checkDir(Vector2 vector)
    {   // Pre: Se llama desde boxTrigger
        // Post: comprueva si en distcheck hay algo
        bool canMove = true;
        float newdist = distCheck / 2;
        //hit = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y) + (vector * newdist), vector, newdist);
        hit = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y), vector, distCheck);
        Debug.DrawRay(new Vector2(this.transform.position.x, this.transform.position.y), vector, Color.red, distCheck);
        if (hit != null)
        {
            if (hit.transform != null)
            {
                if (hit.transform.CompareTag("boxTag"))
                {   // hemos chocado con un enemigo, asi que cambiamos de direcinĦon
                    canMove = false;
                    Debug.Log(Time.deltaTime + " - La caja esta tocando otra caja: " + vector);
                }
            } // if (hit.transform != null)
            else
            {
                if (Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y), vector, distCheck, groundLayer))
                {
                    Debug.Log(Time.deltaTime + " - La caja esta tocando la == PARED ==  " + vector);
                    canMove = false;
                }
            }
        }// if (hit != null)
        
        if (canMove)
        {
            //Debug.Log("Puede moverse");
            newPos = vector;
            isMoving = true;
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("wallTag"))
            isMoving = false;
    }
}
