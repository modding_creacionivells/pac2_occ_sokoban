using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_cs : MonoBehaviour
{
    [SerializeField] private bool isGame;
    [SerializeField] private int camDistNum;
    [SerializeField] private float camVel;
    [SerializeField] private Vector3 initialPos;
    [SerializeField] private Vector3 minValue, maxValue;
    [SerializeField] private Vector3 boundPos;
    [SerializeField]private bool isPause;
    [SerializeField] private GameObject myHero;

    void Awake()
    {
        isPause = false;
        if (isGame)
            isPause = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (isGame && !isPause)
        {
            transform.localPosition = new Vector3(myHero.transform.position.x, myHero.transform.position.y, -10);
        }
        else
        {
            if (!isPause)
            {
                if (Input.mousePosition.x <= camDistNum)                    // izquierda
                    transform.localPosition += new Vector3(1, 0) * camVel * Time.deltaTime * (-1);
                if (Input.mousePosition.x >= Screen.width - camDistNum)     // derecha
                    transform.localPosition += new Vector3(1, 0) * camVel * Time.deltaTime;
                if (Input.mousePosition.y <= camDistNum)                    // abajo
                    transform.localPosition += new Vector3(0, 1) * camVel * Time.deltaTime * (-1);
                if (Input.mousePosition.y >= Screen.height - camDistNum)    // Arriba
                    transform.localPosition += new Vector3(0, 1) * camVel * Time.deltaTime;

                // slow camera 
                boundPos = new Vector3(Mathf.Clamp(transform.position.x, minValue.x, maxValue.x),
                    Mathf.Clamp(transform.position.y, minValue.y, maxValue.y), -10);

                transform.position = boundPos;
                //transform.position = Vector3.Lerp(transform.position, boundPos, camVel * Time.deltaTime);


                if (Input.GetKeyDown(KeyCode.R))
                    transform.localPosition = initialPos;
            }
        }
    }

    public void updateIsPause(bool _isPause) { isPause = _isPause; }

    public void startUsingCam(GameObject _myHero)
    {
        myHero = _myHero;
        isPause = false;
    }
}
