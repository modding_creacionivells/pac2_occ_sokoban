using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class gameManager : MonoBehaviour
{
    [Header("Game Info")]
    [SerializeField] private GameObject myCam;
    [SerializeField] private GameObject myHero;
    [SerializeField] private int numGoals;
    [SerializeField] private int numGoalsUsed;
    [Header("Grid")]
    [SerializeField] private Tilemap tile_map;                  // all walls
    [SerializeField] private Tilemap tile_groundObj;            // ground
    [SerializeField] private Tile tileGround;
    [SerializeField] private Tile tileWall;
    [SerializeField] private Tile tileGoal;
    [SerializeField] private GameObject myBox;
    [SerializeField] private GameObject myPlayer;
    [SerializeField] private GameObject myGoal;

    private List<cellData> cells = new List<cellData>();         

    private saveData _saveData;
    [SerializeField] private Image panelbrightness;
    // Start is called before the first frame update
    void Start()
    {
        string _fileName = "";
        _saveData = new saveData();
        _saveData = saveManager.loadGameState();

        AudioListener.volume = _saveData.sd_Volume;
        panelbrightness.gameObject.SetActive(true);
        panelbrightness.color = new Color(panelbrightness.color.r, panelbrightness.color.g, panelbrightness.color.b, _saveData.sd_brightness);

        numGoals = 0;
        numGoalsUsed = 0;
        switch (_saveData.sd_currentMap)
        {
            case -1:
                _fileName = _saveData.sd_fileName;
                break;
            case 0:
                _fileName = _saveData.sd_map0Name;
                break;
            case 1:
                _fileName = _saveData.sd_map1Name;
                break;
            case 2:
                _fileName = _saveData.sd_map2Name;
                break;
            case 3:
                _fileName = _saveData.sd_map3Name;
                break;
            case 4:
                _fileName = _saveData.sd_map4Name;
                break;
            case 5:
                _fileName = _saveData.sd_map5Name;
                break;
            case 6:
                _fileName = _saveData.sd_map6Name;
                break;
            case 7:
                _fileName = _saveData.sd_map7Name;
                break;
            case 8:
                _fileName = _saveData.sd_map8Name;
                break;
            case 9:
                _fileName = _saveData.sd_map9Name;
                break;
        }
        Debug.Log(Application.persistentDataPath);
        Debug.Log("CurrentMap: " + _saveData.sd_currentMap + " + Name: " + _fileName);
        loadLevel(_fileName);
    }

    private void Update()
    {   // reset del nivel
        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(2); // gameScene
    }

    private Vector3 getXY(int x, int y)
    {   // Pre: se llama cada vez que se va a modificar el terreno
        // Post: obtiene la posicion de X y Y segun nuestro mapa.
        Vector3Int cellPosition = new Vector3Int(x,y,0);
        Vector3 newPos = tile_groundObj.CellToWorld(cellPosition);
        newPos = new Vector3(newPos.x + _saveData.sd_cellSize/2, newPos.y + _saveData.sd_cellSize / 2, 0);    // centrar en la celda
        return newPos;
    }

    public void loadLevel(string fileName)
    {   // Pre: se llama cuando presionamos el boton de load/cargar
        // Post: carga toda la informaci�n que tenemos en gridArray y lo guarda la lsita de celdas
        cells.Clear();
        cells = fileHandle.readFromJSON<cellData>(fileName);
        refreshMap();
        myCam.GetComponent<camera_cs>().startUsingCam(myHero);
    }

    private void refreshMap()
    {
        GameObject allBoxTransform = GameObject.Find("allBox");
        GameObject allGoalsTransform = GameObject.Find("allGoals");
        GameObject myItem;
        Vector3 worldPos;

        for (int x = 0; x < cells.Count; x++)
        {   // 0 ground, 1 wall, 2 goal, 3 prop (box), 4 player 
            worldPos = getXY(cells[x].x, cells[x].y);
            switch (cells[x].value)
            {
                case 0:
                    tile_groundObj.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileGround);
                    break;
                case 1:
                    tile_map.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileWall);
                    break;
                case 2:
                    tile_groundObj.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileGoal);
                    numGoals++;
                    myItem = Instantiate(myGoal, worldPos, Quaternion.identity, allGoalsTransform.transform) as GameObject;
                    break;
                case 3:
                    tile_groundObj.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileGround);
                    myItem = Instantiate(myBox, worldPos, Quaternion.identity, allBoxTransform.transform) as GameObject;
                    break;
                case 4:
                    tile_groundObj.SetTile(new Vector3Int(cells[x].x, cells[x].y, 0), tileGround);
                    myHero = Instantiate(myPlayer, worldPos, Quaternion.identity) as GameObject;
                    break;
            }
        }
    }

    // Pre: Se llama cuando entra una caja a una meta
    // Post: incrementa el contador de cajas
    public void increaseCountGoal() 
    { 
        numGoalsUsed++;
        StartCoroutine(waitingTime());
    }
    // Pre: Se llama cuando sale una caja a una meta
    // Post: decrementa el contador de cajas
    public void decreaseCountGoal() { numGoalsUsed--; }

    public void buttonExit()
    {
        int lvl = 0;
        if (_saveData.sd_currentMap == -1)
        {   // venimos de provar el nivel asi que volvemos a crear
            lvl = 1; // buidLevel
        }
        else
            lvl = 0;
        _saveData.sd_currentMap = 0;
        saveManager.SaveGameState(_saveData);
        SceneManager.LoadScene(lvl); // menu
    }

    IEnumerator waitingTime()
    {   // inicia un contador que hace que el enemigo no pueda hacernos da�o durante 1 segundo
        yield return new WaitForSeconds(1f);
        if (numGoalsUsed == numGoals)
        {
            if (_saveData.sd_currentMap == -1)
            {   // venimos de provar el nivel asi que volvemos a crear
                _saveData.sd_refresh = true;
                _saveData.sd_currentMap = 0;
                saveManager.SaveGameState(_saveData);
                SceneManager.LoadScene(1); // buidLevel
            }
            else
            {
                if (_saveData.sd_currentMap == 9)
                {
                    SceneManager.LoadScene(3); // Finish Game
                }
                else
                {
                    _saveData.sd_currentMap = _saveData.sd_currentMap + 1;
                    saveManager.SaveGameState(_saveData);
                    SceneManager.LoadScene(2); // gameScene
                }
            }
        }
        StopCoroutine(waitingTime());
    }
}
