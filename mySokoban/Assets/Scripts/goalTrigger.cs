using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goalTrigger : MonoBehaviour
{
    private GameObject gameManager;
    void Start()
    {
        gameManager = GameObject.Find("gameManager");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameManager = GameObject.Find("gameManager");
        if (collision.gameObject.CompareTag("boxTag"))
            gameManager.GetComponent<gameManager>().increaseCountGoal();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        gameManager = GameObject.Find("gameManager");
        if (collision.gameObject.CompareTag("boxTag"))
            gameManager.GetComponent<gameManager>().decreaseCountGoal();
    }
}
