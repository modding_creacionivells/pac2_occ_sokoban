using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class bt_lvlSelect : MonoBehaviour
{
    private GameObject mainMenuObj;
    // Start is called before the first frame update
    void Start()
    {
        mainMenuObj = GameObject.Find("sceneManager");
        GetComponent<Button>().onClick.AddListener(showAllLevels);
    }

    // Pre: Se llama al presionar un botton que se muestra en pantalla (son los file names)
    // Post: guarda el nombre del fichero y llama a la confirmacion de si quiere cargar o no el fichero.
    private void showAllLevels() 
    {
        string numeros = this.name.Substring(this.name.Length - 2); // Extrae los caracteres desde la posici�n 4 hasta el final
        int numero = int.Parse(numeros);
        mainMenuObj.GetComponent<mainMenu>().showSelect(numero); 
    }
}
