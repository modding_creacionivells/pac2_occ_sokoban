using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class txt_controller : MonoBehaviour
{
    private GameObject text_object;
    private string txtname;                                     // contains the name of component
    private string[] txtString = new string[3];                 // containts the text we are going to use
    private int txtlanguage;                                    // 0 ENG, 1 ESP, 2 CAT...
    private saveData _saveData;
    
    private void Start()
    {
        _saveData = new saveData();
        _saveData = saveManager.loadGameState();
        chosseText();
    }

    public void chosseText()
    {
        txtname = this.name;
        getLanguage();

        switch (txtname)
        {
            case "txt_title":
                txtString[0] = "Sokoban";
                txtString[1] = "Sokoban";
                txtString[2] = "Sokoban";
                break;
            case "txt_play":
                txtString[0] = "Play Game";
                txtString[1] = "Jugar";
                txtString[2] = "Jugar";
                break;
            case "txt_create":
                txtString[0] = "Create level";
                txtString[1] = "Crear Nivel";
                txtString[2] = "Crear nivell";
                break;
            case "txt_select":
                txtString[0] = "Select level";
                txtString[1] = "Seleccionar nivel";
                txtString[2] = "Seleccionar nivell";
                break;
            case "txt_settings":
                txtString[0] = "Settings";
                txtString[1] = "Opciones";
                txtString[2] = "Opcions";
                break;
            case "txt_credits":
                txtString[0] = "Credits";
                txtString[1] = "Creditos";
                txtString[2] = "Credits";
                break;
            case "txt_floor":
                txtString[0] = "Floor";
                txtString[1] = "Suelo";
                txtString[2] = "Terra";
                break;
            case "txt_wall":
                txtString[0] = "Wall";
                txtString[1] = "Pared";
                txtString[2] = "Pared";
                break;
            case "txt_goal":
                txtString[0] = "Goal";
                txtString[1] = "Meta";
                txtString[2] = "Meta";
                break;
            case "txt_box":
                txtString[0] = "Box";
                txtString[1] = "Caja";
                txtString[2] = "Caixa";
                break;
            case "txt_player":
                txtString[0] = "Player";
                txtString[1] = "Jugador";
                txtString[2] = "Jugador";
                break;
            case "txt_menu":
                txtString[0] = "Menu";
                txtString[1] = "Menu";
                txtString[2] = "Menu";
                break;
            case "txt_fileName":
                txtString[0] = "File Name";
                txtString[1] = "Nombre del fichero";
                txtString[2] = "Nom del fitxer";
                break;
            case "txt_save":
                txtString[0] = "Save Level";
                txtString[1] = "Guardar Nivel";
                txtString[2] = "Guarda Nivell";
                break;
            case "txt_load":
                txtString[0] = "Load Level";
                txtString[1] = "Carga Nivel";
                txtString[2] = "Carga Nivell";
                break;
            case "txt_exit":
                txtString[0] = "Exit";
                txtString[1] = "Salir";
                txtString[2] = "Sortir";
                break;
            case "txt_alert":
                txtString[0] = "Alert!!!";
                txtString[1] = "Alerta!!!";
                txtString[2] = "Alerta!!!";
                break;
            case "txt_conf":
                txtString[0] = "Confirm";
                txtString[1] = "Confirmar";
                txtString[2] = "Confirma";
                break;
            case "txt_cancel":
                txtString[0] = "Cancel";
                txtString[1] = "Cancelar";
                txtString[2] = "Cancela";
                break;
            case "txt_language":
                txtString[0] = "Language";
                txtString[1] = "Idioma";
                txtString[2] = "Idioma";
                break;
            case "txt_Volume":
                txtString[0] = "Volume";
                txtString[1] = "Volumen";
                txtString[2] = "Volum";
                break;
            case "txt_brightness":
                txtString[0] = "Brightness";
                txtString[1] = "Brillo";
                txtString[2] = "Il�luminaci�";
                break;
            case "txt_producer":
                txtString[0] = "Producer";
                txtString[1] = "Productor";
                txtString[2] = "Productor";
                break;
            case "txt_LeadProgramador":
                txtString[0] = "Lead Programmer";
                txtString[1] = "Lider programador";
                txtString[2] = "Cap Programador";
                break;
            case "txt_progamador":
                txtString[0] = "Programmer";
                txtString[1] = "Programador";
                txtString[2] = "Programador";
                break;
            case "txt_designer":
                txtString[0] = "Designer";
                txtString[1] = "Dise�ador";
                txtString[2] = "Dissenyador";
                break;
            case "txt_testMap":
                txtString[0] = "Test Map";
                txtString[1] = "Provar Mapa";
                txtString[2] = "Provar Mapa";
                break;
            case "txt_lvl1":
                txtString[0] = "Level 1: " + _saveData.sd_map0Name;
                txtString[1] = "Nivel 1: " + _saveData.sd_map0Name;
                txtString[2] = "Nivell 1: " + _saveData.sd_map0Name;
                break;
            case "txt_lvl2":
                txtString[0] = "Level 2: " + _saveData.sd_map1Name;
                txtString[1] = "Nivel 2: " + _saveData.sd_map1Name;
                txtString[2] = "Nivell 2: " + _saveData.sd_map1Name;
                break;
            case "txt_lvl3":
                txtString[0] = "Level 3: " + _saveData.sd_map2Name;
                txtString[1] = "Nivel 3: " + _saveData.sd_map2Name;
                txtString[2] = "Nivell 3: " + _saveData.sd_map2Name;
                break;
            case "txt_lvl4":
                txtString[0] = "Level 4: " + _saveData.sd_map3Name;
                txtString[1] = "Nivel 4: " + _saveData.sd_map3Name;
                txtString[2] = "Nivell 4: " + _saveData.sd_map3Name;
                break;
            case "txt_lvl5":
                txtString[0] = "Level 5: " + _saveData.sd_map4Name;
                txtString[1] = "Nivel 5: " + _saveData.sd_map4Name;
                txtString[2] = "Nivell 5: " + _saveData.sd_map4Name;
                break;
            case "txt_lvl6":
                txtString[0] = "Level 6: " + _saveData.sd_map5Name;
                txtString[1] = "Nivel 6: " + _saveData.sd_map5Name;
                txtString[2] = "Nivell 6: " + _saveData.sd_map5Name;
                break;
            case "txt_lvl7":
                txtString[0] = "Level 7: " + _saveData.sd_map6Name;
                txtString[1] = "Nivel 7: " + _saveData.sd_map6Name;
                txtString[2] = "Nivell7: " + _saveData.sd_map6Name;
                break;
            case "txt_lvl8":
                txtString[0] = "Level 8: " + _saveData.sd_map7Name;
                txtString[1] = "Nivel 8: " + _saveData.sd_map7Name;
                txtString[2] = "Nivell 8: " + _saveData.sd_map7Name;
                break;
            case "txt_lvl9":
                txtString[0] = "Level 9: " + _saveData.sd_map8Name;
                txtString[1] = "Nivel 9: " + _saveData.sd_map8Name;
                txtString[2] = "Nivell 9: " + _saveData.sd_map8Name;
                break;
            case "txt_lvl10":
                txtString[0] = "Level 10: " + _saveData.sd_map9Name;
                txtString[1] = "Nivel 10:"  + _saveData.sd_map9Name;
                txtString[2] = "Nivell 10: " + _saveData.sd_map9Name;
                break;
            case "txt_finishGame":
                txtString[0] = "Congratulations!! you finished the game!" ;
                txtString[1] = "��Felicidades!! �Terminaste el juego!";
                txtString[2] = "��Felicitas!! �Has acabat el joc!";
                break;
            case "txt_goBack":
                txtString[0] = "Menu" ;
                txtString[1] = "Menu";
                txtString[2] = "Menu";
                break;
        } // end switch

        text_object = GameObject.Find(txtname);                             // we find the item textmesh
        text_object.GetComponent<TextMeshProUGUI>().text = txtString[txtlanguage];

    }// end chossetext

    private void getLanguage()
    {
        _saveData = new saveData();
        _saveData = saveManager.loadGameState();
        if (_saveData.sd_language == "ENG")
        {
            txtlanguage = 0;
        }
        else if (_saveData.sd_language == "ESP")
        {
            txtlanguage = 1;
        }
        else if (_saveData.sd_language == "CAT")
        {
            txtlanguage = 2;
        }
        else
        {   // english default language
            txtlanguage = 0;
        }

    }
}
