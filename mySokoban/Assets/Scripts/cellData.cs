using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class cellData 
{
    public int x;
    public int y;
    public int value;

    public cellData(int x, int y, int value)
    {
        this.x = x;
        this.y = y;
        this.value = value;
    }
}
