using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class saveData
{
    // this script contains all data we are going to save on our game
    public bool sd_refresh;         // guarda si he refrescado la pantalla
    public string sd_fileName;      // guarda el fichero a refrescar
    public string sd_language;      // guarda el lenguage
    public float sd_Volume;         // volumen
    public float sd_brightness;     // brillo
    public float sd_cellSize;       // guarda el tama�o de la cela
    public int sd_currentMap;       // guarda a que nivel estamos (-1 es que estamos provando un mapa)
    public string sd_map0Name;      // guarda el nombre del mapa 0
    public string sd_map1Name;      // guarda el nombre del mapa 1
    public string sd_map2Name;      // guarda el nombre del mapa 2
    public string sd_map3Name;      // guarda el nombre del mapa 3
    public string sd_map4Name;      // guarda el nombre del mapa 4
    public string sd_map5Name;      // guarda el nombre del mapa 5
    public string sd_map6Name;      // guarda el nombre del mapa 6
    public string sd_map7Name;      // guarda el nombre del mapa 7
    public string sd_map8Name;      // guarda el nombre del mapa 8
    public string sd_map9Name;      // guarda el nombre del mapa 9
}