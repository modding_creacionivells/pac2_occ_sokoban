using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using TMPro;

public class showAllFiles : MonoBehaviour
{
    [SerializeField] private bool isLoad;
    [SerializeField] private GameObject myButton;
    [SerializeField] private Transform scrollViewContent;

    // Start is called before the first frame update
    void Start()
    {
        GameObject newScrollItem;
        int indiceUltimoSeparador = 0;
        string nombreArchivo = "";
        UnityEditor.AssetDatabase.Refresh();
        string[] allFiles = Directory.GetFiles(Application.persistentDataPath);
        for (int i = 0; i < allFiles.Length; i++)
        {
            newScrollItem = Instantiate(myButton, scrollViewContent.position, Quaternion.identity, scrollViewContent) as GameObject;
            indiceUltimoSeparador = allFiles[i].LastIndexOf('\\');
            if (indiceUltimoSeparador != -1) // Verificar si se encontr� el separador
            {
                nombreArchivo = allFiles[i].Substring(indiceUltimoSeparador + 1, allFiles[i].LastIndexOf('.') - indiceUltimoSeparador - 1);
                indiceUltimoSeparador = allFiles[i].LastIndexOf('.');
                string extension = allFiles[i].Substring(indiceUltimoSeparador + 1, 3);
                newScrollItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nombreArchivo;

                if (extension == "txt" || nombreArchivo == "myTestMap") // para que no salga el documento donde guardamos info
                    Destroy(newScrollItem);
            }
            else
                newScrollItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = allFiles[i];

            if (!isLoad)
                newScrollItem.GetComponent<buttonAction>().updateIsSelect();
        }
    }

}
