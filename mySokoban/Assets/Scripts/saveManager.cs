using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class saveManager
{   // esta clase se encarga de crear/modificar/mantener el fichero "saveData.txt" creado por nosotros.
    public static void SaveGameState(saveData _saveData)
    {   // Guarda la informacion que tengamos en el fihero "SaveData.txt"
        BinaryFormatter _binaryFormatter = new BinaryFormatter();
        FileStream myfile;
        myfile = File.Create(Application.persistentDataPath + "/gameInfo.txt");                 // Crea el fichero y si existe lo sobreescribe.
        _binaryFormatter.Serialize(myfile, _saveData);                                          // pone la info en el fichero
        myfile.Close();                                                                         // cierra el fichero
    }

    public static saveData loadGameState()
    {   // Esta funcion carga los datos del juego.
        saveData _saveData = new saveData();
        // Inicializamos las variables por defecto
        _saveData.sd_refresh = false;
        _saveData.sd_fileName = "";
        _saveData.sd_language = "ENG";
        _saveData.sd_Volume = 0;
        _saveData.sd_brightness = 0;    
        _saveData.sd_currentMap = 0;
        _saveData.sd_map0Name = "";
        _saveData.sd_map1Name = "";
        _saveData.sd_map2Name = "";
        _saveData.sd_map3Name = "";
        _saveData.sd_map4Name = "";
        _saveData.sd_map5Name = "";
        _saveData.sd_map6Name = "";
        _saveData.sd_map7Name = "";
        _saveData.sd_map8Name = "";
        _saveData.sd_map9Name = "";
        if (File.Exists(Application.persistentDataPath + "/gameInfo.txt"))
        {
            try
            {
                BinaryFormatter _binaryFormatter = new BinaryFormatter();
                FileStream myfile = File.Open(Application.persistentDataPath + "/gameInfo.txt", FileMode.Open);     // abre el fichero en caso que exista, sino saltaria al catch
                _saveData = (saveData)_binaryFormatter.Deserialize(myfile);                                         // Guarda la informacion en la variable saveData
                myfile.Close();                                                                                     // cierra el fichero una vez guardada la info.
            }
            catch
            {
                // En caso de error podriamos hacer alguna accion pero como ya devolvemos las variables por defecto arriba no hay problema, por eso no hacemos ninguna accion.
            }
        }

        return _saveData;               // devuelve la info obtenida.
    }

}